package org.totalrequiem.quester.quests;

import org.powerbot.script.Tile;
import org.powerbot.script.rt4.Game;
import org.totalrequiem.quester.core.Quest;
import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.impl.Completable;
import org.totalrequiem.quester.core.task.Task;
import org.totalrequiem.quester.core.task.pre.conv.Talk;
import org.totalrequiem.quester.core.task.pre.inv.Use;
import org.totalrequiem.quester.core.task.pre.inv.UseObject;
import org.totalrequiem.quester.core.task.pre.prim.ClickInterface;
import org.totalrequiem.quester.core.task.pre.prim.Interact;
import org.totalrequiem.quester.core.task.pre.prim.WalkTile;
import org.totalrequiem.quester.core.task.pre.tab.OpenTab;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class TutorialIsland extends Quest {

    public TutorialIsland(final Context ctx) {
        super(ctx);
    }

    @Override
    public Task[] tasks() {
        return new Task[] {new Guide(ctx), new Survival(ctx)};
    }

    @Override
    public boolean completed() {
        return false;
    }

    private class Guide extends Task {

        public Guide(final Context ctx) {
            super(ctx);
        }

        @Override
        public boolean completed() {
            return ctx.varpbits.varpbit(406) > 0;
        }

        @Override
        public void run() {

            final Task talk1 = new Talk(ctx, 1548, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.util.fromString("continue") == null && ctx.varpbits.varpbit(1021) == 12;
                }
            }, "continue");

            final Task tab = new OpenTab(ctx, Game.Tab.OPTIONS);


            final Task talk2 = new Talk(ctx, 1548, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.util.fromString("continue") == null;
                }
            }, "continue");

            final Task door = new Interact(ctx, 9398, "Open", Interact.OBJECT, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.players.local().tile().x() > 3097;
                }
            });

            ctx.executor.offer(talk1, tab, talk2, door);


        }
    }

    private class Survival extends Task {

        public Survival(final Context ctx) {
            super(ctx);
        }

        @Override
        public boolean completed() {
            return ctx.varpbits.varpbit(406) > 3;
        }

        @Override
        public void run() {
            final Task walk = new WalkTile(ctx, new Tile(3101, 3095));

            final Task talk1 = new Talk(ctx, 1546, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.util.fromString("Click here to continue") == null && ctx.varpbits.varpbit(1021) == 4;
                }
            }, "Click here to continue");

            final Task tab = new OpenTab(ctx, Game.Tab.INVENTORY);

            final Task interact = new Interact(ctx, 9730, "Chop", Interact.OBJECT, new Completable() {
                @Override
                public boolean completed() {
                    return !ctx.inventory.select().id(2511).isEmpty();
                }
            });

            final Task use = new Use(ctx, 590, 2511, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.varpbits.varpbit(1021) == 2;
                }
            });

            final Task tab2 = new OpenTab(ctx, Game.Tab.STATS);

            final Task talk2 = new Talk(ctx, 1546, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.varpbits.varpbit(406) > 2;
                }
            }, "continue");

            final Task fish = new Interact(ctx, 1557, "Net", Interact.NPC, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.inventory.select().id(2514).size() > 1;
                }
            });

            final Task cook = new UseObject(ctx, 2514, 26185, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.inventory.select().id(315).size() > 0;
                }
            });

            final Task walk2 = new WalkTile(ctx, new Tile(3090, 3091));

            final Task gate = new Interact(ctx, 9470, "Open", Interact.OBJECT, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.varpbits.varpbit(406) > 3;
                }
            });

            ctx.executor.offer(walk, talk1, tab, interact, use, tab2, talk2, fish, cook, walk2, gate);
        }
    }
}
