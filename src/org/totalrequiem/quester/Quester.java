package org.totalrequiem.quester;

import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;
import org.powerbot.script.rt4.Item;
import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.impl.Completable;
import org.totalrequiem.quester.core.obs.list.AnimationListener;
import org.totalrequiem.quester.core.obs.list.InventoryListener;
import org.totalrequiem.quester.core.task.Task;
import org.totalrequiem.quester.core.task.pre.conv.Talk;
import org.totalrequiem.quester.core.task.pre.inv.Use;
import org.totalrequiem.quester.quests.TutorialIsland;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
@Script.Manifest(description = "Test", name = "Test")
public class Quester extends PollingScript<Context> implements AnimationListener, InventoryListener {

    @Override
    public void start() {
        ctx.monitor.start();
        ctx.monitor.offer(this);
        ctx.executor.offer(new TutorialIsland(ctx));

    }

    @Override
    public void poll() {
        final Task current = ctx.executor.peek();
        System.out.println(current);
        if(current != null) {
            if (current.completed()) {
                ctx.executor.remove();
            } else {
                current.run();
            }
        }
    }

    @Override
    public void stop() {
        ctx.monitor.kill();
    }

    @Override
    public void animationChanged(int previous, int current) {
        System.out.println("Animation changed: [" + previous + "; " + current + "]");
    }

    @Override
    public void inventoryChanged(int index, Item previous, Item current) {
        System.out.println("Item changed at " + index + " [" + previous.id() + ", " + current.id() +"]");
    }
}
