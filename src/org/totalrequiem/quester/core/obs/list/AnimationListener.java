package org.totalrequiem.quester.core.obs.list;

import java.util.EventListener;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public interface AnimationListener extends EventListener {

    /**
     * Is called when the players animation changes
     * @param previous previous animation
     * @param current current animation
     */
    public void animationChanged(final int previous, final int current);
}
