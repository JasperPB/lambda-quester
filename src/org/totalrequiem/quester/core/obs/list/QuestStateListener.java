package org.totalrequiem.quester.core.obs.list;

import org.totalrequiem.quester.core.Quest;

import java.util.EventListener;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public interface QuestStateListener extends EventListener {

    /**
     * Is called when a quests' varpbits change
     * @param quest the quest which had its state changed
     * @param previous the previous varpbits value
     * @param current the current varpbits value
     */
    public void stateChanged(final Quest.Quests quest, final int previous, final int current);
}
