package org.totalrequiem.quester.core.obs.list;

import org.powerbot.script.rt4.Item;

import java.util.EventListener;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public interface InventoryListener extends EventListener {

    /**
     * Gets called when an item in the inventory changes (removed, added, increased or decreased)
     * @param index the index at which the change occurred (1 - 28)
     * @param previous The previous Item instance representing the item before the change
     * @param current The current Item instance representing the item after the change
     */
    public void inventoryChanged(final int index, final Item previous, final Item current);

}
