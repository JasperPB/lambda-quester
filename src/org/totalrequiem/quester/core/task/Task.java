package org.totalrequiem.quester.core.task;

import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.context.Provider;
import org.totalrequiem.quester.core.impl.Completable;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public abstract class Task extends Provider implements Completable, Runnable {

    public Task(final Context ctx) {
        super(ctx);
    }

    @Override
    public String toString() {
        return this.getClass().getCanonicalName();
    }
}
