package org.totalrequiem.quester.core.task.pre.conv;

import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.impl.Completable;
import org.totalrequiem.quester.core.task.Task;
import org.totalrequiem.quester.core.task.pre.prim.Interact;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class Talk extends Task {

    private final Task interact, converse;
    private final Completable condition;

    public Talk(final Context ctx, final int id, final Completable condition, final String... options) {
        super(ctx);
        this.interact = new Interact(ctx, id, "Talk", Interact.NPC, new Completable() {
            @Override
            public boolean completed() {
                return ctx.util.fromStrings(options) != null;
            }
        });
        this.converse = new Converse(ctx, options);
        this.condition = condition;
    }

    @Override
    public boolean completed() {
        return condition.completed();
    }

    @Override
    public void run() {
        ctx.executor.offer(interact, converse);
    }
}
