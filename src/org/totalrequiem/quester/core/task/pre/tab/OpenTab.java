package org.totalrequiem.quester.core.task.pre.tab;

import org.powerbot.script.rt4.Game;
import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.task.Task;
import org.totalrequiem.quester.core.task.pre.prim.ClickInterface;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class OpenTab extends Task {

    private final Game.Tab tab;

    public OpenTab(final Context ctx, final Game.Tab tab) {
        super(ctx);
        this.tab = tab;
    }

    @Override
    public boolean completed() {
        return ctx.game.tab() == tab;
    }

    @Override
    public void run() {
        ctx.game.tab(tab);
    }
}
