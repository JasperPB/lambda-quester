package org.totalrequiem.quester.core.task.pre.prim;

import org.powerbot.script.Tile;
import org.powerbot.script.rt4.Player;
import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.impl.Completable;
import org.totalrequiem.quester.core.task.Task;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class WalkTile extends Task {

    private final Tile destination;

    public WalkTile(final Context ctx, final Tile destination) {
        super(ctx);
        this.destination = destination;
    }

    @Override
    public boolean completed() {
        return ctx.players.local().tile().distanceTo(destination) < 5;
    }

    @Override
    public void run() {
        if(ctx.movement.step(destination)) {
            ctx.util.sleepFor(750, new Completable() {
                @Override
                public boolean completed() {
                    final Player local = ctx.players.local();
                    return !local.inMotion() || ctx.movement.destination().distanceTo(local) < 4;
                }
            });
        }
    }
}
