package org.totalrequiem.quester.core.task.pre.prim;

import org.powerbot.script.rt4.Component;
import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.impl.Completable;
import org.totalrequiem.quester.core.task.Task;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class ClickInterface extends Task {

    private final String text;
    private final int widget;
    private final int component;

    public ClickInterface(final Context ctx, final String text) {
        super(ctx);
        this.text = text;
        this.widget = -1;
        this.component = -1;
    }

    public ClickInterface(final Context ctx, final int widget, final int component) {
        super(ctx);
        this.text = "";
        this.widget = widget;
        this.component = component;
    }

    @Override
    public void run() {
        final Component comp;
        if (text.equals("")) {
            comp = ctx.widgets.component(widget, component);
        } else {
            comp = ctx.util.fromString(text);
        }
        if (comp.valid() && comp.visible()) {
            if (comp.click()) {
                ctx.util.sleepFor(500, new Completable() {
                    @Override
                    public boolean completed() {
                        return !comp.valid() || !comp.visible();
                    }
                });
            }
        }
    }

    @Override
    public boolean completed() {
        if(widget != -1) {
            final Component comp = ctx.widgets.component(widget, component);
            return !comp.valid() && !comp.visible();
        } else {
            return ctx.util.fromString(text) == null;
        }
    }

    @Override
    public int hashCode() {
        return text.hashCode();
    }
}
