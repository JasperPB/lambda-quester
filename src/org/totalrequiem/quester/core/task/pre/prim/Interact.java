package org.totalrequiem.quester.core.task.pre.prim;

import org.powerbot.script.rt4.*;
import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.impl.Completable;
import org.totalrequiem.quester.core.task.Task;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class Interact extends Task {

    public final static int NPC = 0;
    public final static int OBJECT = 1;
    public final static int GROUNDITEM = 2;
    public final static int INVENTORY = 3;

    private final int id;
    private final String action;
    private final int type;
    private final Completable condition;


    public Interact(final Context ctx, final int id, final String action, final int type, final Completable condition) {
        super(ctx);
        this.id = id;
        this.action = action;
        this.type = type;
        this.condition = condition;
    }

    public Interact(final Context ctx, final int id, final String action, final int type) {
        this(ctx, id, action, type, new Completable() {
            @Override
            public boolean completed() {
                return ctx.players.local().animation() != -1;
            }
        });
    }

    @Override
    public void run() {
        final BasicQuery<? extends Interactive> query;

        switch (type) {
            case NPC:
                query = ctx.npcs;
                break;

            case OBJECT:
                query = ctx.objects;
                break;

            case GROUNDITEM:
                query = ctx.groundItems;
                break;

            case INVENTORY:
                final Item item = ctx.inventory.select().id(id).peek();
                if(item.interact(action)) {
                    ctx.util.sleepFor(1200, condition);
                }
                return;

            default:
                query = null;
                break;
        }

        final Interactive target = query.select().id(id).nearest().peek();

        if (ctx.players.local().animation() == -1 && target.interact(action)) {
            ctx.util.sleepFor(1200, condition);
        }
    }

    @Override
    public boolean completed() {
        return condition.completed();
    }

}
