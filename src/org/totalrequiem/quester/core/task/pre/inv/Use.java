package org.totalrequiem.quester.core.task.pre.inv;

import org.powerbot.script.rt4.Game;
import org.powerbot.script.rt4.Item;
import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.impl.Completable;
import org.totalrequiem.quester.core.task.Task;
import org.totalrequiem.quester.core.task.pre.prim.Interact;
import org.totalrequiem.quester.core.task.pre.tab.OpenTab;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class Use extends Task {

    private final int item1;
    private final int item2;
    private final Completable completed;

    public Use(final Context ctx, final int item1, final int item2, final Completable completad) {
        super(ctx);
        this.item1 = item1;
        this.item2 = item2;
        this.completed = completad;

    }

    public Use(final Context ctx, final int item1, final int item2) {
        this(ctx, item1, item2, new Completable() {
            @Override
            public boolean completed() {
                return ctx.players.local().animation() != -1;
            }
        });
    }

    @Override
    public boolean completed() {
        return completed.completed();
    }

    @Override
    public void run() {
        final Task[] tasks = {new OpenTab(ctx, Game.Tab.INVENTORY), new Interact(ctx, item1, "Use", Interact.INVENTORY, new Completable() {
            @Override
            public boolean completed() {
                return ctx.inventory.selectedItemIndex() != -1;
            }
        }), new Interact(ctx, item2, "Use", Interact.INVENTORY, new Completable() {
            @Override
            public boolean completed() {
                return ctx.inventory.selectedItemIndex() == -1;
            }
        })};

        ctx.executor.offer(tasks);
    }
}
