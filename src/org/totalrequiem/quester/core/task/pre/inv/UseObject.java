package org.totalrequiem.quester.core.task.pre.inv;

import org.powerbot.script.rt4.Game;
import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.impl.Completable;
import org.totalrequiem.quester.core.task.Task;
import org.totalrequiem.quester.core.task.pre.prim.Interact;
import org.totalrequiem.quester.core.task.pre.tab.OpenTab;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class UseObject extends Task {

    private final int item;
    private final int object;
    private final Completable completed;

    public UseObject(final Context ctx, final int item, final int object, final Completable completed) {
        super(ctx);
        this.item = item;
        this.object = object;
        this.completed = completed;
    }

    @Override
    public boolean completed() {
        return completed.completed();
    }

    @Override
    public void run() {
        ctx.executor.offer(new OpenTab(ctx, Game.Tab.INVENTORY), new Interact(ctx, item, "Use", Interact.INVENTORY, new Completable() {
            @Override
            public boolean completed() {
                return ctx.inventory.selectedItemIndex() != -1;
            }
        }), new Interact(ctx, object, "Use", Interact.OBJECT, new Completable() {
            @Override
            public boolean completed() {
                return ctx.inventory.selectedItemIndex() == -1;
            }
        }));
    }
}
