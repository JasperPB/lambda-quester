package org.totalrequiem.quester.core;

import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.obs.list.QuestStateListener;
import org.totalrequiem.quester.core.task.Task;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public abstract class Quest extends Task {


    public Quest(final Context ctx) {
        super(ctx);
    }

    public abstract Task[] tasks();

    @Override
    public void run() {
        ctx.executor.offer(tasks());
    }

    public enum Quests {
        COOKS_ASSISTANT("Cooks assistant", -1);

        private final String name;
        private final int setting;

        private Quests(final String name, final int setting) {
            this.name = name;
            this.setting = setting;
        }

        public String getName() {
            return name;
        }

        public int getSetting() {
            return setting;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    @Override
    public String toString() {
        return this.getClass().getCanonicalName();
    }
}
