package org.totalrequiem.quester.core.context.ext;

import org.powerbot.script.Filter;
import org.powerbot.script.rt4.Component;
import org.powerbot.script.rt4.Widget;
import org.totalrequiem.quester.core.context.Context;
import org.totalrequiem.quester.core.context.Provider;
import org.totalrequiem.quester.core.impl.Completable;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class Utilities extends Provider {

    /**
     * The constructor for this class
     * @param ctx the context used to pass to the superclass
     */
    public Utilities(final Context ctx) {
        super(ctx);
    }

    /**
     * Sleeps for an amount of time until a condition is met
     * @param millis the maximum amount of time to sleep in milliseconds
     * @param completable the condition to be met
     * @return true if condition was met
     */
    public boolean sleepFor(final long millis, final Completable completable) {
        for (int i = 0; i < 50 && !completable.completed(); i++) {
            sleep(millis / 50);
        }
        return completable.completed();
    }

    /**
     * Sleeps for an amount of time
     * @param millis the amount of time to sleep in milliseconds
     * @return true if successful
     */
    public boolean sleep(final long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            return false;
        }
        return true;
    }


    public Component fromStrings(final String[] strings) {
        for(final String string : strings) {
            final Component comp = fromString(string);
            if(comp != null) {
                return comp;
            }
        }
        return null;
    }

    /**
     * Finds a component based on text
     * @param text the text the component is searched by
     * @return a Component iif found, otherwise null
     */
    public Component fromString(final String text) {
        for(final Widget widget : ctx.widgets.select()) {
            for(final Component component : widget.components()) {
                if(component.valid() && component.text().toLowerCase().contains(text.toLowerCase())) {
                    return component;
                }
            }
        }
        return null;
    }



}
