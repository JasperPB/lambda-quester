package org.totalrequiem.quester.core.context;

import org.powerbot.script.rt4.ClientAccessor;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class Provider extends ClientAccessor {

    /**
     * The Context instance saved so that classes can properly access this Context
     */
    public final Context ctx;

    /**
     * The constructor for this Provider
     * @param ctx the context used to save.
     */
    public Provider(final Context ctx) {
        super(ctx);
        this.ctx = ctx;
    }
}
